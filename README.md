# IG BvC - PoC II - AP 1.2 Standardisiertes Vorgehen mit Open CoDE


Im Rahmen des Arbeitspaketes wurden folgende Aufgaben bearbeitet:

- Aufgabe 1:	Die Vorgaben und Empfehlungen aus der UAG Technik und Betrieb wurden an der Umsetzung von Open CoDE vorbeigeführt vor allem unter dem Aspekt des Rollen- und Rechtekonzeptes. Diese wurden entsprechend für den PoC verwendet und eine entsprechende Struktur für den PoC auf Open CoDE zur Ablage für PoC-Ergebnisse angelegt: https://gitlab.opencode.de/ig-bvc/ig-bvc-poc-2. 
- Aufgabe 2:	Es wurde eine Anleitung zur Nutzung von Open CoDE erstellt mit speziellen Informationen zur Nutzung im Kontext des PoC. Die Nutzung im Kontext von Open CoDE im Kontext des PoC kann auch als Orientierung für andere Projekte und Aktivitäten dienen.   
- Aufgabe 3:	Ferner wurden Anleitungen inklusive Erklärungsvideos erstellt, die den Umgang mit sogenannten „Externen Runnern“ beschreiben. Diese sind erforderlich, wenn Open CoDE nicht nur zur Ablage und Veröffentlichung genutzt werden soll, sondern auch zur aktiven Projektarbeit mit integrierter Nutzung von Entwicklungs-, Test- und Betriebsinfrastrukturen. Die Beschreibungen sind an dieser Stelle hinterlegt:   .
- Aufgabe 4:	Die im Rahmen von Open CoDE aktuell durchgeführten automatisierten Scans zur Sicherung von Qualität der Artefakte im Hinblick auf verwendete Lizenzen und mögliche Schwachstellen wurden aufbereitet und in einer Präsentation zusammengefasst.

Die Ergbnisse und relevanten Dokumente zu den einzelnen Aufgaben sind in entsprechenden Unterverzeichnissen abgelegt.

Das Arbeitspaket wurde zum 31.12.2022 beendet.

